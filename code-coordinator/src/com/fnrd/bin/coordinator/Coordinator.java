package com.fnrd.bin.coordinator;

import com.fnrd.bin.coordinator.controller_server.ControllerServer;
import com.fnrd.bin.coordinator.data_server.DataServer;

import java.util.ArrayList;
import java.util.List;

public class Coordinator {

    public static List<String> servers;
    public static List<String> serversInUse;

    public Coordinator() {
        servers = new ArrayList<>();
        serversInUse = new ArrayList<>();
    }

    public List<String> getServers() {
        return servers;
    }

    public static void main(String[] args) {

        System.out.println("Coordenador Conectado!");
        final Coordinator coordinator = new Coordinator();

        Thread data = new Thread(new Runnable() {
            @Override
            public void run() {

                new DataServer(coordinator);

            }
        });

        Thread controller = new Thread(new Runnable() {
            @Override
            public void run() {

                new ControllerServer(coordinator);

            }
        });

        data.start();
        controller.start();

    }
}
