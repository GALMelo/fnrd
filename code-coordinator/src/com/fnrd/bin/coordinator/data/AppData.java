package com.fnrd.bin.coordinator.data;

import com.fnrd.bin.coordinator.components.ClientFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AppData {

    private final String TAG = AppData.class.getSimpleName();

    private DataConfigurations configurations;
    private List<ClientFile> clientFiles;

    public AppData() {
        configurations = new DataConfigurations();

        try {
            Object obj = configurations.load();
            if ( obj != null) {
                clientFiles = (List<ClientFile>) obj;
                if ( clientFiles != null ) {
                    for ( ClientFile c: clientFiles ) {
                        System.out.println("Arquivos: " + c);
                    }
                } else {
                    clientFiles = new ArrayList<>();
                }

                System.out.println("Arquivos existentes: " + clientFiles.size());
            } else {
                clientFiles = new ArrayList<>();
            }
        } catch ( IOException | ClassNotFoundException e) {
            e.getStackTrace();
        }
    }

    public void save(ClientFile client) {
        clientFiles.add(client);
        configurations.save(clientFiles);
    }

    public void remove(ClientFile client) {
        try {
            Object obj = configurations.load();
            if ( obj != null ) {
                clientFiles = (List<ClientFile>) obj;
                System.out.println("Removendo!");

                for (ClientFile c: clientFiles ) {
                    if ( c.contentEquals(client) ) {
                        clientFiles.remove(c);
                        configurations.save(clientFiles);
                        System.out.println("removido com sucesso: " + clientFiles.size());
                        break;
                    }
                }

                /*
                if ( clientFiles.contains(client) ) {
                    clientFiles.remove(client);
                    System.out.println("removido");
                    configurations.save(clientFiles);
                } */
            }
        } catch ( IOException | ClassNotFoundException e) {
            e.getStackTrace();
        }
    }

    public List<ClientFile> getClientFiles() {
        try {
            if ( clientFiles == null ) {
                clientFiles = new ArrayList<>();
                getConfigurations().save(clientFiles);
            } else {
                clientFiles = (List<ClientFile>) getConfigurations().load();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return clientFiles;
    }

    public DataConfigurations getConfigurations() {
        return configurations;
    }
}
