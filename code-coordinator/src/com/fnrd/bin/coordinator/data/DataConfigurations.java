package com.fnrd.bin.coordinator.data;

import java.io.*;

public class DataConfigurations {

	private File file;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private String path;
	private String directory;
	
	public DataConfigurations() {
		
		// create dir

		path = System.getProperty("user.home");
		System.out.println("path: " + path);
		directory = "/BackupInNetwork/coordinator";

		File fileDir = new File(path + directory);
		
		// check dir and info dir
		if ( !fileDir.exists() ) {
			fileDir.mkdirs();
			System.out.println("File: " + fileDir.getPath());
		}
		
		// create file
		file = new File(path + directory + "/info_clients.save");
	}

	// salva um novo conjunto de dados
	public void save(Object obj) {
		
		try {
			saveInFile(obj);
		} catch(IOException e) {
			e.getStackTrace();
		}
		
	}

	// carrega um conjunto existente de dados
	public Object load() throws IOException, ClassNotFoundException {
		// create input
		Object obj = null;
		
		if ( file.exists() ) {
			input = new ObjectInputStream(new FileInputStream(file));

			 obj = this.input.readObject();

			 input.close();
		}

		return obj;
	}
	
	public void saveInFile(Object obj) throws IOException{
		
		FileOutputStream fileStream = new FileOutputStream(file);
		output = new ObjectOutputStream(fileStream);
		output.writeObject(obj);
		output.close();
		fileStream.close();
		
	}

}
