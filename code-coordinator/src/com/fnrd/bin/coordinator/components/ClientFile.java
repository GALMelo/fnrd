package com.fnrd.bin.coordinator.components;

import java.io.Serializable;

public class ClientFile implements Serializable {

    public static final long serialVersionUID = 1L;

    private int id;
    private String fileName;
    private String nickname;
    private String ipServerFile;

    public ClientFile(int id, String fileName, String nickname, String ipServerFile) {

        this.id = id;
        this.fileName = fileName;
        this.nickname = nickname;
        this.ipServerFile = ipServerFile;

    }

    public boolean contentEquals(ClientFile clientFile) {

        return id == clientFile.id &&
                nickname.equals(clientFile.nickname);


    }

    public ClientFile() {
        this(-1, "", "", "");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getIpServerFile() {
        return ipServerFile;
    }

    public void setIpServerFile(String ipServerFile) {
        this.ipServerFile = ipServerFile;
    }

    @Override
    public String toString() {
        return "ClientFile{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", nickname='" + nickname + '\'' +
                ", ipServerFile='" + ipServerFile + '\'' +
                '}';
    }
}
