package com.fnrd.bin.coordinator.controller_server;

import com.fnrd.bin.coordinator.Coordinator;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

import static com.fnrd.bin.config.ConfigApp.*;

public class ControllerServer {

    private ServerSocket serverSocket;

    public ControllerServer(Coordinator coordinator) {

        try{
            serverSocket = new ServerSocket(PORT_CONTROLLER_COORDINATOR);
            System.out.println("Servidor iniciado na porta: " + PORT_CONTROLLER_COORDINATOR);

            while( true ) {
                Socket client =  serverSocket.accept();
                System.out.println("Um Servidor Conectado!");
                new Thread(new TryClientInControllerServer(client)).start();
            }

        } catch (IOException e) {

            e.getStackTrace();

        }
    }

}
