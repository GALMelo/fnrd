package com.fnrd.bin.coordinator.controller_server;

import com.fnrd.bin.coordinator.Coordinator;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

import static com.fnrd.bin.config.ConfigApp.*;

public class TryClientInControllerServer implements Runnable {

    private Socket client;

    public TryClientInControllerServer(Socket client) {
        this.client = client;
    }

    @Override
    public void run() {

        try {
            PrintStream out = new PrintStream(client.getOutputStream());
            Scanner in = new Scanner(client.getInputStream());

            String ip = client.getInetAddress().getHostAddress();

            String method = in.nextLine(); // Pega o metodo ( register|unregister )
            ip = in.nextLine();
            System.out.println("IP: " + ip);

            if ( method.equalsIgnoreCase(REGISTER) ) {

                // cadastra, caso não esteja cadastrado
                if ( !Coordinator.servers.contains(ip) ) {
                    Coordinator.servers.add(ip);
                    out.println(OK);
                    System.out.println("Servidor cadastrado: " + ip);
                } else {
                    // informa que ja esta cadastrado
                    out.println(FAIL);
                    out.println("Servidor de Arquivos ja cadastrado!");
                }
            } else if ( method.equalsIgnoreCase(UNREGISTER) ) {
                if ( Coordinator.servers.contains(ip) ) {
                    Coordinator.servers.remove(ip);
                    out.println(OK);
                    System.out.println("Servidor descadastrado: " + ip);
                } else {
                    out.println(FAIL);
                    out.println("O servidor nao esta cadastrado!");
                }
            }
            out.close();
            in.close();
            client.close();
            System.out.println("Servidores cadastrados: " + Coordinator.servers.size());
        } catch (IOException e) {
            e.getStackTrace();
        }

    }
}
