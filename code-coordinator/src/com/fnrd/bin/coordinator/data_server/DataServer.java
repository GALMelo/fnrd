package com.fnrd.bin.coordinator.data_server;

import com.fnrd.bin.coordinator.Coordinator;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.fnrd.bin.config.ConfigApp.*;

public class DataServer {

    private ServerSocket serverSocket;
    private Coordinator coordinator;

    public DataServer(Coordinator coordinator) {
        this.coordinator = coordinator;

        try{
            serverSocket = new ServerSocket( PORT_DATA_COORDINATOR );
            System.out.println("Servidor iniciado na porta: " + PORT_DATA_COORDINATOR);

            while( true ) {

                Socket client =  serverSocket.accept();

                System.out.println("Cliente conectado");

                new Thread(new TryClientInDataServer(client)).start();

            }

        } catch (IOException e) {

            e.getStackTrace();

        }
    }




}
