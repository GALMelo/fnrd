package com.fnrd.bin.coordinator.data_server;

import com.fnrd.bin.coordinator.Coordinator;
import com.fnrd.bin.coordinator.components.ClientFile;
import com.fnrd.bin.coordinator.data.AppData;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.fnrd.bin.config.ConfigApp.*;
import static com.fnrd.bin.config.ConfigApp.FAIL;

public class TryClientInDataServer implements Runnable {

    private Socket client;
    private List<String> serversInUsed;
    private AppData appData;

    public TryClientInDataServer(Socket client) {
        this.client = client;
        serversInUsed = new ArrayList<>();
        appData = new AppData();
    }

    @Override
    public void run() {

        try {
            PrintStream out = new PrintStream(client.getOutputStream());

            Scanner in = new Scanner(client.getInputStream());

            String method = in.nextLine(); //recebe o método

            if (method.equalsIgnoreCase(SEND)) {
                trySendClient(out, in);
            } else if ( method.equalsIgnoreCase(LIST) ) {
                tryListClient(out, in);
            } else if ( method.equalsIgnoreCase(GET) ) {
                tryGetClient(out, in);
                System.out.println("Baixando");
            }


        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    private void tryGetClient(PrintStream out, Scanner in) throws IOException {

        String nickname = in.nextLine();
        String id = in.nextLine();

        for ( ClientFile clientFile: appData.getClientFiles() ) {
            if ( Integer.parseInt(id) == clientFile.getId() && nickname.equals(clientFile.getNickname())) {
                tryGetFileToClient(out, in, clientFile);
                return;
            }
        }

        out.println(FAIL);
        out.println("Nenhum servidor existente com esse arquivo!");

    }

    private void tryGetFileToClient(PrintStream out, Scanner in, ClientFile clientFile) throws IOException {

        if ( Coordinator.servers.contains(clientFile.getIpServerFile()) ) {
            String serverIp = clientFile.getIpServerFile();

            Socket socket = new Socket(clientFile.getIpServerFile(), PORT_DATA_SERVERFILE);
            PrintStream outServer = new PrintStream(socket.getOutputStream());
            Scanner inServer = new Scanner(socket.getInputStream());

            outServer.println(GET);
            outServer.println(clientFile.getId());

            int response = inServer.nextInt();
            inServer.nextLine();

            if ( response == OK ) {
                inServer.nextLine();
                long dataLength = Long.parseLong(inServer.nextLine());

                String dataServer = "";

                while (inServer.hasNext()) {
                    dataServer += inServer.nextLine();
                    System.out.println("Dados recebidos: " + dataServer.length());
                    if ( dataLength == dataServer.length() ) break;
                }

                System.out.println("data: " + dataServer.length());

                //String data = inServer.nextLine();
                out.println(OK);
                out.println(clientFile.getFileName());
                out.println("");
                out.println(""+dataLength);
                int lengthDefault = 1048576;

                if ( dataServer.length() > lengthDefault ) {
                    int sends = dataServer.length() / lengthDefault;
                    System.out.println(sends);
                    for ( int i = 0; i < sends; i++) {

                        if ( i == sends-1) {
                            int v = i*lengthDefault;
                            String block = dataServer.substring(v);
                            out.println(block);
                            System.out.println("Finish Upload...");
                        } else {
                            int v = i*lengthDefault;
                            String block = dataServer.substring(v, v+lengthDefault);
                            out.println(block);
                            if ( i % 10 == 0 ) {
                                System.out.println("Upload...");
                            }
                        }
                    }
                } else {
                    out.println(dataServer);
                }
                appData.remove(clientFile);
                System.out.println("Cliente: " + clientFile + " foi removido!");
                //out.println(data);
            }

        } else {
            out.println(FAIL);
            out.println("Nenhum servidor existente com esse arquivo!");
        }
    }

    private void tryListClient(PrintStream out, Scanner in) throws IOException {
        String nickname = in.nextLine();
        System.out.println(appData.getClientFiles());
        if ( appData.getClientFiles().size() > 0 ) {
            out.println(OK);

            for ( ClientFile clientFile: appData.getClientFiles() ) {
                String id = String.valueOf(clientFile.getId());
                String fileName = clientFile.getFileName();

                if ( nickname.equals(clientFile.getNickname()) ) {
                    out.println(id + " - " + fileName);
                }

            }
        } else {
            out.println(FAIL);
            out.println("Nenhum arquivo existente!");
        }
        out.close();
        in.close();
        client.close();
    }

    private void trySendClient(PrintStream out, Scanner in) throws IOException{
        ClientFile clientFile = new ClientFile();
        String fileName = in.nextLine();
        String nickname = in.nextLine();

        clientFile.setFileName(fileName);
        clientFile.setNickname(nickname);

        in.nextLine();

        long dataLength = Long.parseLong(in.nextLine());

        //String data = in.nextLine();
        String data = "";

        while (in.hasNext()) {
            data += in.nextLine();
            System.out.println("Dados recebidos: " + data.length());
            if ( dataLength == data.length() ) break;
        }

        System.out.println("data: " + data.length());

        trySendToServerFile(clientFile, out, in, data, 0);
    }

    private void trySendToServerFile(ClientFile clientFile, PrintStream out, Scanner in, String data, int call) throws IOException {
        if ( Coordinator.servers.size() > 0 ) {

            String serverIp = null;

            if ( Coordinator.serversInUse.size() > 0 ) {
                for ( String server: Coordinator.servers ) {
                    if ( Coordinator.serversInUse.contains(server) ) continue;
                    serverIp = server;
                    Coordinator.serversInUse.add(server);
                    break;
                }
            } else {
                serverIp = Coordinator.servers.get(0);
                Coordinator.serversInUse.add(serverIp);
            }

            System.out.println("IP: " + serverIp);

            /*
            for ( String server: Coordinator.servers ) {
                if ( !serversInUsed.contains(server)) {
                    serverIp = server;
                    break;
                }
            } */

            if ( serverIp != null ) {

                System.out.println("serverIP: " + serverIp );
                Socket socket = new Socket(serverIp, PORT_DATA_SERVERFILE);
                PrintStream outServer = new PrintStream(socket.getOutputStream());
                Scanner inServer = new Scanner(socket.getInputStream());

                int id = (int) (Math.random() * 10000);
                clientFile.setId(id);
                clientFile.setIpServerFile(
                                    serverIp
                        );

                outServer.println(SEND);
                outServer.println(id);
                outServer.println();
                //outServer.println(data);
                outServer.println("" + data.length());

                //send
                int lengthDefault = 1048576;

                if ( data.length() > lengthDefault ) {
                    int sends = data.length() / lengthDefault;
                    System.out.println(sends);
                    for ( int i = 0; i < sends; i++) {

                        if ( i == sends-1) {
                            int v = i*lengthDefault;
                            String block = data.substring(v);
                            outServer.println(block);
                            System.out.println("Finish Upload...");
                        } else {
                            int v = i*lengthDefault;
                            String block = data.substring(v, v+lengthDefault);
                            outServer.println(block);
                            long currentValue = data.substring(0, v).length();
                            //System.out.println("Valor faltando: " + currentValue);
                            if ( i % 10 == 0 ) {
                                System.out.println("Upload... " + ( ( currentValue * 100)/data.length() ) + "%" );
                            }
                        }
                    }
                } else {
                    outServer.println(data);
                }

                int cod = inServer.nextInt();
                inServer.nextLine();

                if ( cod == OK ) {
                    System.out.println("Arquivo enviado com sucesso!");
                    out.println(OK);
                    out.println(id);
                    appData.save(clientFile);
                    System.out.println(clientFile.toString());
                } else if ( cod == FAIL ) {
                    out.println(FAIL);
                    out.println("Erro ao enviar o arquivo! Tente novamente!");
                }


            } else if (call < 5) {
                trySendToServerFile(clientFile, out, in, data, call + 1);
            } else {
                out.println(FAIL);
                out.println("Erro ao enviar o arquivo! Tente novamente!");
            }

            Coordinator.serversInUse.remove(serverIp);
        } else {
            out.println(FAIL);
            out.println("Nenhum servidor conectado. Tente novamente outra hora!");
        }

    }
}
