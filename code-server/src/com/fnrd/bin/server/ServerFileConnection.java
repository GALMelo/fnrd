package com.fnrd.bin.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import static com.fnrd.bin.config.ConfigApp.*;

public class ServerFileConnection {

    private ServerSocket serverSocket;

    public ServerFileConnection() {
        try {
            serverSocket = new ServerSocket(8000);
            String myIp = InetAddress.getLocalHost().getHostAddress();
            System.out.println(myIp);

            while (true) {
                Socket client = serverSocket.accept();

                new Thread(new TryClientInServerFile(client)).start();
            }

        } catch (IOException e) {
            e.getStackTrace();
        }
    }

}
