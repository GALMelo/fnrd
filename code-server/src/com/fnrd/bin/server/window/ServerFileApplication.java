package com.fnrd.bin.server.window;

import com.fnrd.bin.server.ServerFileConnection;
import com.fnrd.bin.utils.BINBaseUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.util.Scanner;

import static com.fnrd.bin.config.ConfigApp.*;

public class ServerFileApplication extends JFrame {

    private static final int WIDTH = 600; // largura da janela
    private static final int HEIGHT = 300; // altura da janela

    private static ServerFileApplication frame; // janela

    // icons/images
    private static final URL CONFIG_PATH = ServerFileApplication.class.getResource("/assets/icons/ic_configurations.png");
    private static final URL IP_PATH = ServerFileApplication.class.getResource("/assets/icons/ic_ip.png");
    private static final URL ICON_PATH = ServerFileApplication.class.getResource("/assets/icons/icon_app.png");

    private JMenuItem miIpCoordinator;
    private String ipCoordinator = "127.0.0.1";
    private JMenuItem miQuit;

    private JButton action;
    private JLabel ipInfo;
    private boolean isRegister = false;

    public ServerFileApplication() {
        try {
            // aplica a interface do sistema na aplicacao
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            create();

        } catch ( Exception ex ) {
            ex.getStackTrace();
        }
    }

    private void create() {
        // Configuracoes da janela
        setTitle("BackupInNetwork - Servidor de Arquivos");
        setResizable(false);
        setLayout(null);
        setSize(WIDTH, HEIGHT);
        setIconImage(Toolkit.getDefaultToolkit().getImage(ICON_PATH));
        setBackground(Color.WHITE);

        setJMenuBar(createMenuBar());

        initElements();
    }

    private void initElements() {
        int bntWidth = 200;
        int bntHeight = 120;

        action = BINBaseUtils.createButton("CADASTRAR", 20, getHeight()/2 - bntHeight + 20, bntWidth, bntHeight, 20, Font.BOLD);
        ipInfo = new JLabel("IP: " + ipCoordinator);
        ipInfo.setFont(ipInfo.getFont().deriveFont(Font.BOLD, 16));
        ipInfo.setBackground(Color.RED);
        ipInfo.setBounds(action.getX() + action.getWidth() + 20, action.getY(), (int)(bntWidth*(5/3f)), bntHeight);
        add(action, SwingConstants.CENTER);
        add(ipInfo);

        addMenuEvent();
        addButtonEvent();
        unregister();
    }

    private JMenuBar createMenuBar() {


        JMenuBar menuBar = new JMenuBar();
        JMenu configurations = new JMenu("");
        configurations.setIcon(BINBaseUtils.newImageIcon(CONFIG_PATH, 30, 30));
        miIpCoordinator = new JMenuItem("IP.Coord [ " + ipCoordinator + " ]",
                BINBaseUtils.newImageIcon(IP_PATH, 20, 20));

        miQuit = new JMenuItem("Desconectar");

        configurations.add(miIpCoordinator);
        configurations.add(miQuit);

        menuBar.add(configurations);

        return menuBar;
    }

    private void addButtonEvent() {

        action.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if ( !isRegister ) {
                    boolean sucess = register();
                    if ( sucess )  {
                        isRegister = true;
                        action.setText("DESCADASTRAR");
                    } else {
                        JOptionPane.showMessageDialog(frame, "Erro ao se cadastrar");
                    }

                } else {
                    boolean sucess = unregister();
                    if ( sucess ) {
                        isRegister = false;
                        action.setText("CADASTRAR");
                    } else {
                        JOptionPane.showMessageDialog(frame, "Erro ao se cadastrar");
                    }
                }

            }
        });

    }

    public void addMenuEvent() {
        miIpCoordinator.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // cria um dialogo para pegar o ip do coordenador
                String opIP = (String) JOptionPane.showInputDialog(null, "Digite o IP do Coordenador: ", "IP Coordenador", JOptionPane.QUESTION_MESSAGE,
                        BINBaseUtils.newImageIcon(IP_PATH, 50, 50), null, null);

                if ( opIP != null ) {
                    ipCoordinator = opIP;
                }

                System.out.println("IP: " + ipCoordinator);
                if ( !ipCoordinator.isEmpty() ) {
                    miIpCoordinator.setText("IP.Coord [ " + ipCoordinator + " ]");
                    ipInfo.setText("IP: " + ipCoordinator);
                }
            }
        });

        miQuit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean unregistred = unregister();

                if ( unregistred ) {
                    System.exit(0);
                }
            }
        });
    }


    public boolean unregister() {
        boolean sucess = false;
        try {
            Socket socket = new Socket(ipCoordinator, PORT_CONTROLLER_COORDINATOR);

            if ( socket.isConnected() ) {
                PrintStream out = new PrintStream(socket.getOutputStream());
                Scanner in = new Scanner(socket.getInputStream());
                out.println(UNREGISTER);
                String myIp = InetAddress.getLocalHost().getHostAddress();
                out.println(myIp);
                int cod = in.nextInt();

                if ( cod == OK ) {
                    System.out.println("Servidor descadastrado com sucesso!");
                    sucess = true;
                } else if ( cod == FAIL ) {
                    in.nextLine();
                    String message = in.nextLine();
                    System.out.println("Message: " + message);
                    sucess = true;
                }
                out.close();
                in.close();
            }
            socket.close();
        } catch (IOException e) {
            e.getStackTrace();
        }
        return sucess;
    }

    public boolean register() {
        boolean sucess = false;
        try {
            Socket socket = new Socket(ipCoordinator, PORT_CONTROLLER_COORDINATOR);
            if (socket.isConnected()) {
                PrintStream out = new PrintStream(socket.getOutputStream());
                Scanner in = new Scanner(socket.getInputStream());

                out.println(REGISTER);
                String myIp = InetAddress.getLocalHost().getHostAddress();
                out.println(myIp);
                int cod = in.nextInt();
                in.nextLine();

                if ( cod == OK ) {
                    System.out.println("Servidor cadastrado com sucesso!");

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            new ServerFileConnection();
                        }
                    }).start();
                    sucess = true;
                } else if ( cod == FAIL ) {
                    String message = in.nextLine();
                    System.out.println("Message: " + message);
                }
                out.close();
                in.close();
            }
            socket.close();

        } catch (IOException ioException) {
            ioException.getStackTrace();
        }

        return sucess;
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // Cria a janela principal
                frame = new ServerFileApplication();
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                Dimension window = Toolkit.getDefaultToolkit().getScreenSize();
                frame.setLocation( (window.width - frame.getSize().width) / 2, (window.height - frame.getSize().height) / 2);
                frame.setVisible(true);

            }
        });

    }


}
