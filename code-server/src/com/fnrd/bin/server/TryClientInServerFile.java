package com.fnrd.bin.server;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

import static com.fnrd.bin.config.ConfigApp.*;

public class TryClientInServerFile implements Runnable {

    private Socket client;

    public TryClientInServerFile(Socket client) {
        this.client = client;
    }

    @Override
    public void run() {

        try {
            PrintStream out = new PrintStream(client.getOutputStream());
            Scanner in = new Scanner(client.getInputStream());

            String method = in.nextLine();

            if ( method.equalsIgnoreCase(SEND)) {

                int id = in.nextInt();
                in.nextLine();
                in.nextLine();
                System.out.println("id: " + id);
                //String data = in.nextLine();
                long dataLength = Long.parseLong(in.nextLine());

                //String data = in.nextLine();
                String data = "";

                while (in.hasNext()) {
                    data += in.nextLine();
                    System.out.println("Dados recebidos: " + data.length());
                    if ( dataLength == data.length() ) break;
                }

                System.out.println("data: " + data.length());

                writeFile(id, data);

                out.println(OK);

            } else if ( method.equalsIgnoreCase(GET) ) {

                String id = in.nextLine();

                String sdir = System.getProperty("user.home") + "/BackupInNetwork/server";
                File file = new File(sdir + "/" + id + ".stmp");

                if ( file.exists() ) {
                    out.println(OK);

                    byte[] byteFile = new FileInputStream(file).readAllBytes();
                    String data = encodeBase64(byteFile);
                    out.println("");
                    System.out.println("data: " + data.length());
                    out.println(""+data.length());

                    int lengthDefault = 1048576;

                    if ( data.length() > lengthDefault ) {
                        int sends = data.length() / lengthDefault;
                        System.out.println(sends);
                        for ( int i = 0; i < sends; i++) {

                            if ( i == sends-1) {
                                int v = i*lengthDefault;
                                String block = data.substring(v);
                                out.println(block);
                                System.out.println("Finish Upload...");
                            } else {
                                int v = i*lengthDefault;
                                String block = data.substring(v, v+lengthDefault);
                                out.println(block);
                                long currentValue = data.substring(0, v).length();
                                //System.out.println("Valor faltando: " + currentValue);
                                if ( i % 10 == 0 ) {
                                    System.out.println("Upload... " + ( ( currentValue * 100)/data.length() ) + "%" );
                                }
                            }
                        }
                    } else {
                        out.println(data);
                    }
                    //out.println(data);

                }

            }

            out.close();
            in.close();
            client.close();

        } catch (IOException e) {
            e.getStackTrace();
        }

    }

    private void writeFile(int id, String data) throws IOException {

        String sdir = System.getProperty("user.home") + "/BackupInNetwork/server";
        String file = sdir + "/" + id + ".stmp";

        File dir = new File(sdir);
        if ( dir != null && !dir.exists() ) {
            dir.mkdirs();
        }

        byte[] dataByte = decodeBase64(data);

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(dataByte);
        fileOutputStream.close();
    }
}
