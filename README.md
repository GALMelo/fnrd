# FNRD

Projeto criado para matéria de Fundamento de Rede de Computadores.

* **Equipe:**

* Mikhael de Oliveira Silva D'Amato

* Gabriel Melo

* Rodrigo Nogueira

* **Versões:** Java 11 (https://jdk.java.net/java-se-ri/11)
* **IDE:** Intellij IDEA (https://www.jetbrains.com/pt-br/idea/download/#section=linux)

* **DOC:** Protocolos do Projeto - (https://docs.google.com/document/d/1lWP8Bp8DV8TwnkjK0I_tnk64v9tHtcBHTa62EtBu4AA/edit?ts=5fa2f822)

* **DOC:** Relatório do projeto - (https://docs.google.com/document/d/1S6o1AV9RCy4-S9vwjZ2TCikiHD6-_-0LcWiS4JnXkNo/edit?usp=sharing) 



