package com.fnrd.bin.config;

import java.util.Base64;

public class ConfigApp {


    // Portas
    public static int PORT_DATA_COORDINATOR = 10000;
    public static int PORT_CONTROLLER_COORDINATOR = 9999;
    public static int PORT_DATA_SERVERFILE = 8000;

    // Metodo de requisicao
    public static String SEND = "SEND";
    public static String LIST = "LIST";
    public static String GET = "GET";
    public static String REGISTER = "REGISTER";
    public static String UNREGISTER = "UNREGISTER";

    // Métodos de resposta
    public static int OK = 200;
    public static int FAIL = 404;

    public static String encodeBase64(byte[] data) {
        return Base64.getEncoder().encodeToString(data);
    }

    public static byte[] decodeBase64(String data) {
        return Base64.getDecoder().decode(data);
    }
}
