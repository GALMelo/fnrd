package com.fnrd.bin.utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;

public class BINBaseUtils {

    /***
     *
     * @apiNote Utilidades para a construcao de janelas e objetos para as aplicacoes.
     *
     * */

    protected BINBaseUtils() { }

    /***
     *
     * @param resource - se refere a localizacao da imagem
     * @param width - se refere a altura relativa da imagem
     * @param height - se refere a altura relativa da imagem
     * @return icon - retorna o icone criado
     */
    public static ImageIcon newImageIcon(URL resource, int width, int height) {
        ImageIcon icon = new ImageIcon(resource);
        icon.setImage(icon.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
        return icon;
    }

    /***
     *
     *  -- Generico criador de botoes --
     *
     * @param text - Se refere ao texto do botao
     * @param x - se refere a posicao x relativa do botao ao container pai
     * @param y - se refere a posicao y relativa do botao ao container pai
     * @param w - se refere a largura relativa do botao
     * @param h - se refere a altura relativa do botao
     * @param fontSize - se refere ao tamanho da fonte para o texto do botao
     * @param fontStyle - se refere ao estilo da fonte ( plain, bold, italic )
     * use Font.(style) para pegar o estilo apropriado
     * @param backgroundColor - se refere a cor de fundo do botao
     * @param foregroundColor - se refere a cor dos elementos dentro do botao
     *
     * @return - retorna um botao
     */

    public static JButton createButton(String text, int x, int y, int w, int h, int fontSize, int fontStyle,
                                       Color backgroundColor, Color foregroundColor) {

        JButton bnt = new JButton(text);
        bnt.setBounds(x, y, w, h);
        bnt.setBorderPainted(false);
        bnt.setFocusPainted(false);
        bnt.setContentAreaFilled(false);
        bnt.setOpaque(true);
        bnt.setBackground(backgroundColor);
        bnt.setForeground(foregroundColor);
        bnt.setFont(bnt.getFont().deriveFont(fontStyle, fontSize));

        bnt.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
                bnt.setBackground(Color.decode("#444444"));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e);
                bnt.setBackground(Color.decode("#000000"));
            }
        });
        return bnt;
    }

    public static JButton createButton(String text, int x, int y, int w, int h, int fontSize, int fontStyle) {
        return createButton(text, x, y, w, h, fontSize, fontStyle, Color.decode("#000000"), Color.WHITE);
    }

    public static JButton createButton(String text, int x, int y, int w, int h, int fontSize) {
        return createButton(text, x, y, w, h, fontSize, Font.PLAIN);
    }

    public static JButton createButton(String text, int x, int y, int w, int h) {
        return createButton(text, x, y, w, h, 16);
    }

}
