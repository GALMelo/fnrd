package com.fnrd.bin.client.data;

import com.fnrd.bin.client.components.Client;

import java.io.IOException;

public class AppData {

    private final String TAG = AppData.class.getSimpleName();

    private DataConfigurations configurations;
    private Client client;

    public AppData() {
        configurations = new DataConfigurations();

        try {
            client = configurations.load();
        } catch ( IOException | ClassNotFoundException e) {
            e.getStackTrace();
        }
    }

    public void save(Client client) {
        configurations.save(client);
    }

    public Client getClient() {
        return client;
    }

    public DataConfigurations getConfigurations() {
        return configurations;
    }
}
