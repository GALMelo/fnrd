package com.fnrd.bin.client.data;

import com.fnrd.bin.client.components.Client;

import java.io.*;

public class DataConfigurations {
	
	private File file;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private String path;
	private String directory;
	
	public DataConfigurations() {
		
		// create dir

		path = System.getProperty("user.home");
		System.out.println("path: " + path);
		directory = "/BackupInNetwork/client";

		File fileDir = new File(path + directory);
		
		// check dir and info dir
		if ( !fileDir.exists() ) {
			fileDir.mkdirs();
			System.out.println("File: " + fileDir.getPath());
		}
		
		// create file
		file = new File(path + directory + "/configurations.save");
		
	}

	// salva um novo conjunto de dados
	public void save(Client client) {
		
		try {
			saveInFile(client);
		} catch(IOException e) {
			e.getStackTrace();
		}
		
	}

	// carrega um conjunto existente de dados
	public Client load() throws IOException, ClassNotFoundException {
		// create input
		Client client = null;
		
		if ( file.exists() ) {
			input = new ObjectInputStream(new FileInputStream(file));

			 client = (Client) this.input.readObject();
			System.out.println("Client carregado: " + client);

			 input.close();
		}

		if ( client == null ) {
			client = new Client();
		}

		return client;
	}
	
	public void saveInFile(Client client) throws IOException{
		
		FileOutputStream fileStream = new FileOutputStream(file);
		output = new ObjectOutputStream(fileStream);
		System.out.println("Salvando Cliente: " + client);
		output.writeObject(client);
		output.close();
		fileStream.close();
		
	}

}
