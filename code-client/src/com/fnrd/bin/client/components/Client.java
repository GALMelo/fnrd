package com.fnrd.bin.client.components;

import java.io.Serializable;

import static com.fnrd.bin.config.ConfigApp.*;

public class Client implements Serializable {

    public static final long serialVersionUID = 1L;

    private String nickname;
    private String ipCoordinator;
    private int port;
    private String dirDownload;

    public Client() {
        nickname = "Undefined";
        ipCoordinator = "127.0.0.1";
        port = PORT_DATA_COORDINATOR;
        dirDownload = "" + System.getProperty("user.home") + "/Downloads";

    }

    // == Getters and Setters ==

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getIpCoordinator() {
        return ipCoordinator;
    }

    public void setIpCoordinator(String ipCoordinator) {
        this.ipCoordinator = ipCoordinator;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDirDownload() {
        return dirDownload;
    }

    public void setDirDownload(String dirDownload) {
        this.dirDownload = dirDownload;
    }

    @Override
    public String toString() {
        return "Client{" +
                "nickname='" + nickname + '\'' +
                ", ipCoordinator='" + ipCoordinator + '\'' +
                ", port=" + port +
                ", dirDownload='" + dirDownload + '\'' +
                '}';
    }
}
