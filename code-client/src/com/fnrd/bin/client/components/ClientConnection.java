package com.fnrd.bin.client.components;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.fnrd.bin.config.ConfigApp.*;


public class ClientConnection {

    public static String sendFile(Client client, String fileName, byte[] data) {

        if ( client != null ) {
            try {
                Socket socket = new Socket(client.getIpCoordinator(), client.getPort());

                if ( socket != null && socket.isConnected() ) {
                    PrintStream out = new PrintStream(socket.getOutputStream());
                    Scanner in = new Scanner(socket.getInputStream());

                    if ( socket.isConnected() ) {

                        out.println(SEND); // Metodo
                        out.println(fileName); // nome do arquivo
                        out.println(client.getNickname()); // apelido
                        out.println(); // linha em branco
                        String dataStr = encodeBase64(data); // codifica bytes para string
                        System.out.println("data: " + dataStr.length());
                        out.println(""+dataStr.length());

                        int lengthDefault = 1048576;

                        if ( dataStr.length() > lengthDefault ) {
                            int sends = dataStr.length() / lengthDefault;
                            System.out.println(sends);
                            for ( int i = 0; i < sends; i++) {

                                if ( i == sends-1) {
                                    int v = i*lengthDefault;
                                    String block = dataStr.substring(v);
                                    out.println(block);
                                    System.out.println("Finish Upload... 100%");
                                } else {
                                    int v = i*lengthDefault;
                                    String block = dataStr.substring(v, v+lengthDefault);
                                    out.println(block);
                                    long currentValue = dataStr.substring(0, v).length();
                                    //System.out.println("Valor faltando: " + currentValue);
                                    if ( i % 10 == 0 ) {
                                        System.out.println("Upload... " + ( ( currentValue * 100)/dataStr.length() ) + "%" );
                                    }
                                }
                            }
                        } else {
                            out.println(dataStr);
                        }

                        //out.println(dataStr);

                        int cod = in.nextInt();
                        System.out.println(cod);

                        if ( cod == OK ) {
                            int idFile = in.nextInt();
                            System.out.println(idFile);
                            out.close();
                            in.close();
                            socket.close();
                            return "ID do arquivo: " + idFile;
                        } else if ( cod == FAIL ) {
                            in.nextLine();
                            String response = in.nextLine();
                            System.out.println(response);
                            out.close();
                            in.close();
                            socket.close();
                            return response;
                        }

                    }
                    out.close();
                    in.close();
                    socket.close();
                } else {
                    JOptionPane.showMessageDialog(
                            null,
                            "Erro ao enviar! Conexão não foi estabilizada!",
                            "Mensagem da Aplicação", JOptionPane.INFORMATION_MESSAGE);
                }

            } catch (IOException e) {
                e.getStackTrace();
                System.out.println(String.format("Erro ao enviar o arquivo em %s:%d. ",
                        client.getIpCoordinator(), client.getPort()) + e.getMessage());

                JOptionPane.showMessageDialog(
                        null,
                        "Erro ao enviar! Conexão não foi estabilizada!",
                        "Mensagem da Aplicação", JOptionPane.INFORMATION_MESSAGE);
            }
        }

        return null;
    }

    public static List<String> listFiles(Client client) {

        List<String> list = new ArrayList<>();

        if ( client != null ) {
            try {
                System.out.println("Porta: " + client.getPort());
                Socket socket = new Socket(client.getIpCoordinator(), client.getPort());

                if ( socket != null && socket.isConnected() ) {
                    PrintStream out = new PrintStream(socket.getOutputStream());
                    Scanner in = new Scanner(socket.getInputStream());

                    out.println(LIST);
                    out.println(client.getNickname());

                    int cod = in.nextInt();
                    in.nextLine();
                    if ( cod == OK ) {

                        while ( in.hasNext() ) {
                            list.add(in.nextLine());
                        }

                        out.close();
                        in.close();
                        socket.close();

                        return list;
                    } else if ( cod == FAIL ) {

                        String response = in.nextLine();

                        JOptionPane.showMessageDialog(
                                null,
                                "Erro ao listar!: " + response,
                                "Mensagem do Coordenador", JOptionPane.INFORMATION_MESSAGE);

                        out.close();
                        in.close();
                        socket.close();
                        return list;

                    }
                } else {
                    JOptionPane.showMessageDialog(
                            null,
                            "Erro ao listar! Conexão não foi estabilizada!",
                            "Mensagem da Aplicação", JOptionPane.INFORMATION_MESSAGE);
                }

            } catch (IOException e) {
                e.getStackTrace();
                JOptionPane.showMessageDialog(
                        null,
                        "Erro ao listar! Conexão não foi estabilizada!",
                        "Mensagem da Aplicação", JOptionPane.INFORMATION_MESSAGE);
            }
        }

        return list;
    }

    public static void getFile(Client client, String id) {

        if ( client != null ) {
            try {
                Socket socket = new Socket(client.getIpCoordinator(), client.getPort());

                if ( socket != null && socket.isConnected() ) {
                    PrintStream out = new PrintStream(socket.getOutputStream());
                    Scanner in = new Scanner(socket.getInputStream());

                    out.println(GET);
                    out.println(client.getNickname());
                    out.println(id);

                    int cod = in.nextInt();

                    if ( cod == OK ) {


                        in.nextLine();

                        String fileName = in.nextLine();
                        in.nextLine();

                        //String data = in.nextLine();
                        long dataLength = Long.parseLong(in.nextLine());
                        System.out.println("dataLength: " + dataLength);

                        String data = "";

                        while (in.hasNext()) {
                            data += in.nextLine();
                            System.out.println("Dados recebidos: " + data.length());
                            if ( dataLength == data.length() ) break;
                        }

                        System.out.println("data: " + data.length());

                        writeFile(client.getDirDownload(), fileName, data);
                        System.out.println("Download completo!");
                        JOptionPane.showMessageDialog(
                                null,
                                "O Arquivo: " + fileName + " foi baixado com sucesso!",
                                "Mensagem do Coordenador", JOptionPane.INFORMATION_MESSAGE);

                    } else if ( cod == FAIL ) {

                        String response = in.nextLine();
                        JOptionPane.showMessageDialog(
                                null,
                                "Erro ao Baixar!: " + response,
                                "Mensagem do Coordenador", JOptionPane.INFORMATION_MESSAGE);
                    }
                    out.close();
                    in.close();
                    socket.close();
                } else {
                    JOptionPane.showMessageDialog(
                            null,
                            "Erro ao baixar! Conexão não foi estabilizada!",
                            "Mensagem da Aplicação", JOptionPane.INFORMATION_MESSAGE);
                }

            } catch (IOException e) {
                e.getStackTrace();
                JOptionPane.showMessageDialog(
                        null,
                        "Erro ao baixar! Conexão não foi estabilizada!",
                        "Mensagem da Aplicação", JOptionPane.INFORMATION_MESSAGE);
            }

        }
    }

    private static void writeFile(String path, String name, String data) throws IOException {
        System.out.println("writing...");
        String file = path + "/" + name;

        File dir = new File(file);

        byte[] dataByte = decodeBase64(data);

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(dataByte);
        fileOutputStream.close();
    }

}
