package com.fnrd.bin.client.window;

import com.fnrd.bin.client.components.Client;
import com.fnrd.bin.client.components.ClientConnection;
import com.fnrd.bin.client.data.AppData;
import com.fnrd.bin.utils.BINBaseUtils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static com.fnrd.bin.config.ConfigApp.*;

public class ClientApplication extends JFrame {

    private static final int WIDTH = 600; // largura da janela
    private static final int HEIGHT = 600; // altura da janela

    private static ClientApplication frame; // janela

    // icons/images
    private static final URL CONFIG_PATH = ClientApplication.class.getResource("/assets/icons/ic_configurations.png");
    private static final URL IP_PATH = ClientApplication.class.getResource("/assets/icons/ic_ip.png");
    private static final URL DIR_PATH = ClientApplication.class.getResource("/assets/icons/ic_dir.png");
    private static final URL PERSON_PATH = ClientApplication.class.getResource("/assets/icons/ic_person.png");
    private static final URL ICON_PATH = ClientApplication.class.getResource("/assets/icons/icon_app.png");

    // menu items
    private JMenuItem miNickname;
    private JMenuItem miIpCoordinator;
    private JMenuItem miDirDownload;
    private JMenuItem miQuit;

    // buttons
    private JButton bntUpload;
    private JButton bntListener;
    private JButton bntDownload;

    // list
    private JList<String> listFiles;

    // popups
    private Popup popupSend;
    private Popup popupDir;
    private PopupFactory popupFactory;

    // cliente
    private Client client;

    // persistencia de dados
    private AppData appData;

    public ClientApplication() {

        try {
            // aplica a interface do sistema na aplicacao
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            create();
        } catch ( Exception ex ) {
            ex.getStackTrace();
        }

        appData = new AppData();
        client = appData.getClient();

        miNickname.setText("Apelido [ " + client.getNickname() + " ]");
        miIpCoordinator.setText("IP.Coord [ " + client.getIpCoordinator() + " ]");

    }

    // -- Inicializa todos os Elementos e Configuracoes da Janela --

    private void create() {

        // Configuracoes da janela
        setTitle("BackupInNetwork - Cliente");
        setResizable(false);
        GridLayout layout = new GridLayout(1, 2, 1, 1);
        setLayout(layout);
        setSize(WIDTH, HEIGHT);
        setIconImage(Toolkit.getDefaultToolkit().getImage(ICON_PATH));
        setBackground(Color.WHITE);

        popupFactory = new PopupFactory();

        add(createContainerLeft());
        add(createContainerRight());

        setJMenuBar(createMenuBar());

    }

    // -- Cria um Container com os botoes principais --

    private JPanel createContainerRight() {
        JLabel lblList = new JLabel("Lista de Arquivos");
        lblList.setOpaque(true);
        lblList.setHorizontalAlignment(SwingConstants.CENTER);
        lblList.setBackground(Color.BLACK);
        lblList.setForeground(Color.WHITE);
        lblList.setFont(lblList.getFont().deriveFont(Font.BOLD, 20));
        lblList.setBorder(new EmptyBorder(10, 0, 10, 0));


        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout(0, 10));
        panel.add(lblList, BorderLayout.NORTH);
        panel.add(createListScrollView(), BorderLayout.CENTER);
        panel.setBackground(Color.WHITE);
        panel.setBorder(new EmptyBorder(10, 0, 10, 10));

        return panel;
    }

    // -- Cria um Container com a lista --

    private JPanel createContainerLeft() {

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 1, 10, 10));

        bntUpload = BINBaseUtils.createButton("Upload", 20, 20, 100, 50, 25);
        bntListener = BINBaseUtils.createButton("Listar", 20, 20, 100, 50, 25);
        bntDownload = BINBaseUtils.createButton("Download", 20, 20, 100, 50, 25);

        panel.add(bntUpload);
        panel.add(bntListener);
        panel.add(bntDownload);
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        panel.setBackground(Color.WHITE);

        return panel;
    }

    // -- Gerador de itens para lista --

    private DefaultListModel<String> createList(List<String> list) {
        DefaultListModel<String> listModel = new DefaultListModel<>();

        if ( list != null ) {
            for (String s: list) {
                listModel.addElement(s);
            }
        }

        return listModel;
    }

    // -- Criador da lista com scroll vertical --

    private JScrollPane createListScrollView() {

        listFiles = new JList<>(createList(null));

        listFiles.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                showMenu(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                showMenu(e);
            }

            private void showMenu(MouseEvent e) {
                createMenuToCopy(e);
            }
        });

        listFiles.setCellRenderer(createCellRendererToList());

        listFiles.setBorder(new EmptyBorder(10, 10, 10, 10));

        JScrollPane scrollPane = new JScrollPane(listFiles);
        scrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        scrollPane.setBackground(Color.WHITE);

        return scrollPane;
    }

    // -- Creador de como uma celula da lista sera renderizada --

    private DefaultListCellRenderer createCellRendererToList() {
        DefaultListCellRenderer renderer = new DefaultListCellRenderer(){
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                JLabel lbl = new JLabel(value.toString());
                lbl.setFont(lbl.getFont().deriveFont(Font.BOLD, 16));
                lbl.setOpaque(true);
                lbl.setHorizontalAlignment(SwingConstants.LEADING);
                lbl.setMaximumSize(new Dimension(WIDTH / 2, HEIGHT));
                list.setBackground(Color.WHITE);
                if ( !isSelected) {
                    lbl.setForeground(Color.BLACK);
                    lbl.setBackground(Color.WHITE);
                } else {
                    lbl.setForeground(Color.WHITE);
                    lbl.setBackground(Color.BLACK);
                }

                return lbl;
            }
        };

        return renderer;
    }

    // -- Criador do menu para copiar textos da lista --

    private void createMenuToCopy(MouseEvent e) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();


        if ( e.isPopupTrigger() ) {
            JPopupMenu popupMenu = new JPopupMenu();
            JMenuItem copy = new JMenuItem("copiar id");
            copy.setHorizontalAlignment(SwingConstants.LEFT);
            JMenuItem copyAll = new JMenuItem("copiar tudo");
            copyAll.setHorizontalAlignment(SwingConstants.LEFT);

            copy.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    String text = listFiles.getSelectedValue().split(" - ")[0];

                    StringSelection selection = new StringSelection(text);
                    clipboard.setContents(selection, null);
                }
            });

            copyAll.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    StringSelection selection = new StringSelection(listFiles.getSelectedValue());
                    clipboard.setContents(selection, null);
                }
            });

            popupMenu.add(copy);
            popupMenu.add(copyAll);
            listFiles.setSelectedIndex(listFiles.locationToIndex(e.getPoint()));
            popupMenu.show(listFiles, e.getX(), e.getY());
        }
    }

    // -- Criador da barra de menu --

    private JMenuBar createMenuBar() {


        JMenuBar menuBar = new JMenuBar();
        JMenu configurations = new JMenu("");
        configurations.setIcon(BINBaseUtils.newImageIcon(CONFIG_PATH, 30, 30));

        miNickname = new JMenuItem("",
                BINBaseUtils.newImageIcon(PERSON_PATH, 20, 20));
        miIpCoordinator = new JMenuItem("",
                BINBaseUtils.newImageIcon(IP_PATH, 20, 20));
        miDirDownload = new JMenuItem("Download",
                BINBaseUtils.newImageIcon(DIR_PATH, 20, 20));

        miQuit = new JMenuItem("Sair");

        configurations.add(miNickname);
        configurations.add(miIpCoordinator);
        configurations.add(miDirDownload);
        configurations.add(miQuit);

        addMenuEvents();
        addButtonsEvents();

        menuBar.add(configurations);

        return menuBar;
    }

    // -- Adicionar eventos dos botoes --

    private void addButtonsEvents() {

        bntUpload.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Upload de Arquivo");
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                fileChooser.showOpenDialog(frame);

                File file = fileChooser.getSelectedFile();

                try {
                    if ( file != null ) {
                        String name = file.getName();
                        System.out.println("Enviando o arquivo: " + name);
                        byte[] data =  new FileInputStream(file).readAllBytes();
                        String response = ClientConnection.sendFile(client, file.getName(), data);

                        if ( response != null && !response.isEmpty() ) {

                            JOptionPane.showMessageDialog(frame, response,
                                    "Mensagem do Servidor", JOptionPane.INFORMATION_MESSAGE);
                        }
                    }

                } catch (IOException e2) {
                    e2.getStackTrace();
                } catch (OutOfMemoryError eOut) {
                    eOut.getStackTrace();
                    //TODO nova forma de pegar o arquivo
                }
            }
        });

        bntListener.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<String> list = ClientConnection.listFiles(client);

                if ( list != null ) {
                    listFiles.setModel(createList(list));
                }

            }
        });

        bntDownload.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String id = JOptionPane.showInputDialog(
                        frame, "Informe o ID do arquivo: ",
                        "Identificador", JOptionPane.QUESTION_MESSAGE);

                if ( id != null && !id.isEmpty() && !id.isBlank() ) {
                    ClientConnection.getFile(client, id);
                }

            }
        });

    }

    private Popup createPopup(String text, int x, int y) {
        JLabel lbl = new JLabel(text);
        lbl.setOpaque(true);
        lbl.setBackground(Color.BLACK);
        lbl.setForeground(Color.decode("#EEEEEE"));
        lbl.setFont(lbl.getFont().deriveFont(Font.BOLD, 20));
        lbl.setBorder(new EmptyBorder(10, 10, 10, 10));;

        Popup popup = popupFactory.getPopup(frame, lbl, x, y);
        return popup;
    }

    // -- Configurador dos eventos do menu --

    private void addMenuEvents() {

        miNickname.addActionListener(e -> {
            // cria um dialogo para pegar o apelido
            String opNick = (String) JOptionPane.showInputDialog(null, "Digite seu apelido: ", "APELIDO", JOptionPane.QUESTION_MESSAGE,
                    BINBaseUtils.newImageIcon(PERSON_PATH, 48, 48), null, client.getNickname());

            System.out.println("Apelido: " + opNick);
            if ( opNick != null && !opNick.trim().isEmpty() ) {
                miNickname.setText("Apelido [ " + opNick + " ]");
                client.setNickname(opNick);
                appData.save(client);
            }
        });

        miIpCoordinator.addActionListener( e -> {
            // cria um dialogo para pegar o ip do coordenador
            String opIP = (String) JOptionPane.showInputDialog(null, "Digite o IP do Coordenador: ", "IP Coordenador", JOptionPane.QUESTION_MESSAGE,
                    BINBaseUtils.newImageIcon(IP_PATH, 64, 64), null, null);

            if ( opIP != null && !opIP.isEmpty() ) {
                miIpCoordinator.setText("IP.Coord [ " + opIP + " ]");
                client.setIpCoordinator(opIP);
                appData.save(client);
            }

        });

        miDirDownload.addActionListener( e -> {

            // abre a janela de diretorios do sistema
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Diretório de Download");
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);


            if ( !client.getDirDownload().isEmpty() ) {
                fileChooser.setSelectedFile(new File(client.getDirDownload()));
                fileChooser.setCurrentDirectory(new File(client.getDirDownload()));
            }

            fileChooser.showOpenDialog(null);
            File file = fileChooser.getSelectedFile();

            client.setDirDownload( file != null ? file.getPath() : client.getDirDownload() );
            if ( popupDir != null ) popupDir.hide();
            appData.save(client);


        });

        miDirDownload.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e);

                if ( popupDir != null ) popupDir.hide();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);

                // informa o diretorio atual de download
                JLabel lbl = new JLabel("Dir: " + client.getDirDownload());
                lbl.setOpaque(true);
                lbl.setBackground(Color.BLACK);
                lbl.setForeground(Color.decode("#EEEEEE"));
                lbl.setFont(lbl.getFont().deriveFont(Font.BOLD, 10));
                lbl.setBorder(new EmptyBorder(5, 5, 5, 5));;

                int x = (int) (
                        frame.getX() + e.getX() + 30
                        );
                int y = (int) (
                        e.getLocationOnScreen().y + 20
                        );

                popupDir = popupFactory.getPopup(frame, lbl, x, y);
                popupDir.show();

            }
        });

        miQuit.addActionListener(e -> System.exit(0));

    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // Cria a janela principal
                frame = new ClientApplication();
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                Dimension window = Toolkit.getDefaultToolkit().getScreenSize();
                frame.setLocation( (window.width - frame.getSize().width) / 2, (window.height - frame.getSize().height) / 2);
                frame.setVisible(true);

            }
        });

    }

}
